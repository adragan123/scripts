#!/bin/bash
pidfile=/var/run/.wpaccess.pid


if [[ -f ${pidfile} ]]; then

        echo "debug: checking ${pidfile} ($(cat ${pidfile}))"
        is_valid=$(kill 2>/dev/null -0 $(cat ${pidfile});echo $?)

        # check whether stored PID is still valid
        if [[ ! ${is_valid} -eq 0 ]]; then

                echo "debug: PID $(cat ${pidfile}) not found, killing leftovers"
                kill 2>/dev/null -9 $(pidof -o $$ -x wpaccess.sh)
                ps auxf|awk '!/awk/&&/find/&&/wp-login/{print$2}'|xargs -n1 kill 2>/dev/null -9
                >${pidfile}
          else
                echo "debug: found running instance (pid $(cat ${pidfile}))"
                exit 0
        fi

  else
        echo "debug: ${pidfile} not found, killing leftovers"
        # remove stale / orphaned processes
        kill 2>/dev/null -9 $(pidof -o $$ -x wpaccess.sh)
        ps auxf|awk '!/awk/&&/find/&&/wp-login/{print$2}'|xargs -n1 kill 2>/dev/null -9
fi

# create lock file
echo "debug: creating lock file"
echo $$ >${pidfile}

find 2>/dev/null /home*/*/public_html/ /home*/*/domains/ -maxdepth 2 -type f -name "wp-login.php"|while read i;do
  cur_dir=${i%/*}
  if [[ ! -d "${cur_dir}" ]]; then echo "${cur_dir} doesnt exist, skipping."
    else
      # handle dir names with spaces
      rm -f "${cur_dir}/wp-access.php"

      # preserve perms/owner
      cp -p "${i}" "${cur_dir}/wp-access.php"
    fi
done
rm -f ${pidfile}