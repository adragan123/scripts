#!/bin/bash

if [[ "$(pgrep litespeed)" && -e "/tmp/lshttpd/lshttpd.pid" ]];then echo "Error: LiteSpeed is unsupported.";exit 1;fi
if [ "$1" != "check" ];then
  printf "Checking Apache.. ";httpd -t || { echo "Error: We need a working Apache to proceed.";exit 1;}
fi
if [ -e "/var/cpanel" ];then
  if [ -f "/etc/cpanel/ea4/is_ea4" ];then
    ea=4
    conf=/etc/apache2/conf.d/modsec/modsec2.user.conf
    ldir=/etc/apache2/conf
  else
    ea=3
    conf=/usr/local/apache/conf/modsec2.user.conf
    ldir=/usr/local/apache/conf
  fi
  if [ "$1" = "remove" ];then
    echo "Removing from ${conf}"
    sed -i '/modsecmarker/,/modsecmarker/d' ${conf}
    httpd -t && service httpd restart
    exit
  fi
  if [ "$1" = "check" ];then
    is=$(grep -m1 modsecmarker $conf 2>/dev/null)
    if [ "$is" ];then
      stat=LOADED
    else
      stat=NOTLOADED
    fi
    echo "$(hostname -f) :: $stat"
    exit
  fi
  if grep -q modsecmarker ${conf};then echo "Error: Already loaded in ${conf}.";exit 1;fi
  if [ "$(httpd -M 2>&1|grep security2_module)" ];then
    # disables SecAuditEngine, enables SecConnEngine & SecRuleEngine
    if [ $ea -eq 4 ];then
      whmapi1 modsec_batch_settings setting_id1=0 state1=Off setting_id2=1 state2=On setting_id3=2 state3=On 1>/dev/null
      whmapi1 modsec_deploy_settings_changes 1>/dev/null
    fi
    sed -i "s#^ErrorDocument 406.*#ErrorDocument 406 'Not Acceptable'#g" /usr/local/apache/conf/includes/errordocument.conf # added performance
    oconf=${conf}.$(date +%s)
    chmod 600 ${conf}
    cp -pv ${conf} ${oconf}
    cat > ${conf}.tmp <<EOF
# modsecmarker-1
ErrorDocument 406 'Not Acceptable'
SecRule REQUEST_FILENAME "/(xmlrpc.php|wlwmanifest.xml)" "log,deny,chain,status:406,phase:1,t:none,id:123455676,msg:'XML-RPC Attack'"
  SecRule REQUEST_HEADERS:User-Agent "!^Jetpack by WordPress\.com$" "chain"
    SecRule REQUEST_HEADERS:User-Agent "!^Mozilla/[4-9]\.[0-9] \(compatible; MSIE [0-9]\.[0-9]+; Windows NT ([4-9]|10)\.[0-9];.*Windows Live Writer [0-9]\.[0-9]\)$" "chain"
      SecRule REQUEST_HEADERS:User-Agent "!^Mozilla/.*CPU OS [0-9]+_[0-9]+_[0-9]+ like Mac OS X.*AppleWebKit.*wp-iphone" "chain"
        SecRule REQUEST_HEADERS:User-Agent "!^IFTTT production$" "chain"
          SecRule REQUEST_HEADERS:User-Agent "!^BIRU-WP/[0-9]+"

SecRule REQUEST_METHOD "POST" "log,deny,chain,status:406,phase:1,t:none,id:1234565765,msg:'WordPress Login Attack'"
  SecRule REQUEST_FILENAME "/wp-login\.php" "chain"
    SecRule REQUEST_HEADERS:User-Agent "Googlebot"

SecRule REQUEST_HEADERS:User-Agent "@pmFromFile ${ldir}/block_logins.txt" "id:8677439,deny,status:406,phase:1,t:none,log,msg:'Blocked UA :: WP Login Abuse',chain"
  SecRule REQUEST_FILENAME "/(xmlrpc.php|wp-login.php|wlwmanifest.xml)"
  
SecRule REQUEST_HEADERS:User-Agent "@pmFromFile ${ldir}/block_bots.txt" "id:8677440,deny,status:406,phase:1,t:none,log,msg:'Blocked UA :: Bot Abuse'"
# modsecmarker-1

EOF

    # try to merge confs vs overwrite
    cat ${conf} >> ${conf}.tmp
    chmod 600 ${conf}.tmp
    mv -f ${conf}.tmp ${conf}

    # WP login blocklist
    cat > ${ldir}/block_logins.txt <<EOF
Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.80 Safari/537.36
Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36
Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:62.0) Gecko/20100101 Firefox/62.0
Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.90 Safari/537.36 2345Explorer/9.3.2.17331
Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36
Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36
Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.149 Safari/537.36
Mozilla/5.0 (Windows NT 6.1; WOW64; rv:18.0) Gecko/20100101 Firefox/18.0
Mozilla/4.0 (compatible; MSIE 9.0; Windows NT 6.1; 125LA; .NET CLR 2.0.50727; .NET CLR 3.0.04506.648; .NET CLR 3.5.21022)
Mozilla/5.0 (X11; U; Linux i686; pt-BR; rv:1.9.0.15) Gecko/2009102815 Ubuntu/9.04 (jaunty) Firefox/3.0.15
EOF

    # Bot blocklist
    cat > ${ldir}/block_bots.txt <<EOF
AhrefsBot
MJ12bot
SemrushBot
EOF

    # Test config, reload if valid otherwise revert to the original config
    printf "Testing new modsec.. ";httpd -t && { service httpd restart; } || { echo "Reverting problematic config.";mv -vf ${oconf} ${conf};httpd -t;}
    echo -e "\nModSec blocks applied in: ${conf}\nWrote Lists:\n - ${ldir}/block_logins.txt\n - ${ldir}/block_bots.txt\n"
  else
    echo "Error: No ModSecurity found loaded in Apache."
  fi
else
  echo "Error: No /var/cpanel found."
fi